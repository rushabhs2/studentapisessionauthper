from django.apps import AppConfig


class AuthperapiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'authperapi'
